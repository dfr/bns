Feature: Shop page

    Scenario: No shops in db
        When I go to '/shop'
        Then I should not see 'Записаться на снятие замеров'

    Scenario: Have shop in db
        Given I have '1' shop
        When I go to '/shop'
        Then I should see 'Записаться на снятие замеров'

    @javascript
    Scenario: All days closed by default
        Given I have '1' shop
        When I go to '/shop'
        And I click 'Записаться на снятие замеров'
        Then I should see all days closed

    @javascript
    Scenario: Opened one day in admin
        Given I have '1' shop
        And I have opened current day in admin
        When I go to '/shop'
        And I click 'Записаться на снятие замеров'
        Then I should see one day open

    @javascript
    Scenario: User takes one day
        Given I have '1' shop
        And I have opened current day in admin
        When I go to '/shop'
        And I click 'Записаться на снятие замеров'
        And I click free cell
        And I click 'book' button
        Then I should have today measure busy

