Feature: Register new user
    In order to log in user should first register

    @javascript
    Scenario: Submit empty form
        When I go to homepage
        And I click 'мой аккаунт'
        And I click 'создать аккаунт'
        And I click button 'создать аккаунт'
        Then I should see 'fio не может быть пустым'
        And I should see 'email не может быть пустым'

    @javascript
    Scenario: Submit bad email form
        When I go to homepage
        And I click 'мой аккаунт'
        And I click 'создать аккаунт'
        And I fill 'fio' with 'Иванов Иван Иванович'
        And I fill 'phone' with '81234567890'
        And I fill 'email' with 'deflexor1ya.ru'
        And I fill 'password' with '1234'
        When I click button 'создать аккаунт'
        Then I should see 'email имеет неверное значение'

    @javascript
    Scenario: Submit good email form
        When I go to homepage
        And I click 'мой аккаунт'
        And I click 'создать аккаунт'
        And I fill 'fio' with 'Иванов Иван Иванович'
        And I fill 'phone' with '81234567890'
        And I fill 'email' with 'deflexor@ya.ru'
        And I fill 'password' with '1234'
        When I click button 'создать аккаунт'
        Then I should see 'Вам отправлено письмо'

