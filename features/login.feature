Feature: Login

    @javascript
    Scenario: Submit empty form
        When I go to homepage
        And I click 'мой аккаунт'
        And I click button 'Войти'
        Then I should see 'Неверный email или пароль'

    @javascript
    Scenario: Submit unconfirmed user
        Given I have a user "test@test.com" with password "somepass"
        When I go to the homepage
        And I click 'мой аккаунт'
        And I fill in "user[email]" with "test@test.com"
        And I fill in "user[password]" with "somepass"
        And I click button 'Войти'
        Then I should see 'Вам необходимо подтвердить ваш аккаунт'

    @javascript
    Scenario: Submit confirmed user
        Given I have a user "test@test.com" with password "somepass"
        And user "test@test.com" is confirmed
        When I go to the homepage
        And I click 'мой аккаунт'
        And I fill in "user[email]" with "test@test.com"
        And I fill in "user[password]" with "somepass"
        And I click button 'Войти'
        Then I should see 'Успешная авторизация'

