Feature: Administer users

    Scenario: Count
        Given I have a '3' random users
        When I go to '/admin/users'
        Then I should see '( 3 )'

    @javascript
    Scenario: Quicksearch with autocomplete (pass only with selenium)
        Given I have a user "test@test.com" with fio 'Hello Test' and password "somepass"
        When I go to '/admin/users'
        And I fill in "q" with "tes"
        Then I should see 'Hello Test'

    Scenario: View all
        Given I have a '3' random users
        When I go to '/admin/users'
        And I click 'Показать всех'
        Then I should see list of '3' users

