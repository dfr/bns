# encoding: utf-8

Given /^I have '(\d+)' shops?$/ do |n|
  #:address, :time, :phone, :title
  @shops = (1..n.to_i).map do |i|
    addr = "#{Random.country}, #{Random.address_line_1}"
    Shop.create! :phone => Random.international_phone,
      :time => Random.date.to_s,
      :address => addr,
      :title => "Магазин на набережной"
  end
end

Given /^I have opened current day in admin$/ do
  @measure_today = @shops[0].shop_measures.create(:at => DateTime.now, :isbusy => false)
end

Then /^I should see all days closed$/ do
  page.should have_css('.sizing_table td.current_day', :count => 84)
end

Then /^I should see one day open$/ do
  page.should have_css('.sizing_table td.current_day', :count => 83)
end

When /^I click free cell$/ do
  find('.sizing_table td:not(.current_day)').click
end

Then /^I should see popup closed$/ do
  #find('.sizing_table').visible?.should be_false
  page.should have_no_css('.sizing_table td')
end

Then /^I should have today measure busy$/ do
  @measure_today.isbusy.should be_true
end

When /^I click 'book' button$/ do
  find('.m_phone a').click
end
