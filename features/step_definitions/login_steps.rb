
Given /^I have a user "(.*?)" with password "(.*?)"$/ do |email, password|
  @user = User.create!(:phone => '1234567890', :fio => 'test',
                       :email => email, :password => password, :password_confirmation => password)
end

When /^I fill in "(.*?)" with "(.*?)"$/ do |fld, val|
  fill_in fld, :with => val
end

Given /^user "(.*?)" is confirmed$/ do |arg1|
  @user.confirm!
end

