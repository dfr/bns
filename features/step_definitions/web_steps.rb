When /^I go to (?:the )?homepage$/ do
  visit('/')
end

When /^I go to '(.+?)'$/ do |url|
  visit(url)
end


Given /^I click '(.+)'$/ do |link_text|
  click_link(link_text)
end

Given /^I click button '(.+)'$/ do |button_text|
  click_button(button_text)
end

When /^I click on '(.+)' '(.+)'$/ do |id, link_text|
  within(id) do
    click_link(link_text)
  end
end

Then /^I should see '(.*?)'$/ do |text|
  #page.status_code == 200
  #puts page.html
  #assert page.has_content?(text), "page does not have this content: #{text}"
  page.should have_content(text)
end

Then /^I should not see '(.*?)'$/ do |text|
  page.should have_no_content(text)
end
