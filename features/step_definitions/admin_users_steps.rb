Given /^I have a '(\d+)' random users$/ do |n|
  @users = (1..n.to_i).map { |i| User.create!(:email => "#{i}#{Random.email}", :fio => "#{Random.firstname} #{Random.initial} #{Random.lastname}", :phone => Random.international_phone, :password => Random.alphanumeric(5)) }
end

Given /^I have a user "(.*?)" with fio '(.*?)' and password "(.*?)"$/ do |email, fio, password|
  @user = User.create!(:phone => '1234567890', :fio => fio,
                       :email => email, :password => password, :password_confirmation => password)
end

Then /^I should see list of '(\d+?)' users$/ do |n|
  page.should have_css('.user_list > table > tbody > tr', :count => n)
end
