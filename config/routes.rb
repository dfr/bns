Bns::Application.routes.draw do
  resources :articles

  devise_scope :user do
    match '/user/confirmation' => 'confirmations#update', :via => :put, :as => :update_user_confirmation
    resource :registration,
      only: [:new, :create, :edit, :update],
      path: 'users',
      path_names: { new: 'sign_up' },
      controller: 'devise/registrations',
      as: :user_registration
  end
  devise_for :users, :skip => [:registrations], :controllers => { :confirmations => "confirmations" }

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  namespace :admin do
    root :to => 'home#index'
    resources :fabrics
    resources :accessories
    resources :articles, :except => [:new,:destroy] do
      post 'preview', :on => :collection
    end
    resources :users
    resources :shops do
      get 'day_measures', :on => :member
    end
    post 'shop_measure_setbusy' => "shop_measure#set_busy"
    post 'shop_measure_set_username' => "shop_measure#set_username"
  end

  namespace :buy do
    %w(suit jacket trousers shirt coat).each do |cloth|
      get "#{cloth}(/:step)" => "#{cloth}#index", :as => "#{cloth}"
      get "select_#{cloth}_fabric/:id" => "#{cloth}#select_fabric"
      post "select_#{cloth}_design" => "#{cloth}#select_design"
    end
  end

  get "about" => "home#about"
  get "profile" => "home#profile"
  get "shop" => "shop#index"
  get "shop/sizing_table" => "shop#sizing_table"
  post "shop/book_hour" => "shop#book_hour"
  get "faq" => "home#faq"
  get "gift_certificates" => "home#gift_certificates"
  get "quality_guarantee" => "home#quality_guarantee"
  get "material" => "home#material"
  get "process" => "home#process"
 # buy online pages
  get "buy_online" => "home#buy_online"

  get "content/:p1(/:p2)" => "home#content"

  root :to => 'home#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
