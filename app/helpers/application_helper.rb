# encoding: utf-8
require 'translit'
#require 'kramdown'

module ApplicationHelper
  MONTHS = %w/- январь февраль март апрель май июнь июль август сентябрь октябрь ноябрь декабрь/

  def transliterate(text)
    Translit.convert(text)
  end

  def textile(text)
    RedCloth.new(text).to_html.html_safe
  end

  def ru_month_name(month)
    MONTHS[month]
  end

  # Devise helpers
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
