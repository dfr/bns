jQuery(function($) {


    $('#design_form select').on('change', function () {
        var special_cases = {
            'suit_fason': 'suit_fason#0_#1_#2'
        };
        var id = $(this).attr('id');
        var opt_idx = id.match(/\d+$/)[0]
        // text info
        $('#span_' + id).text( $(this).find(':selected').text() );
        // visual preview
        $('.blank_suit div').css('visibility', 'hidden');

        for(var i in DesignStruct) {
            var clset = DesignStruct[i];
            var sel_idx = $('#d_opt_' + i).prop('selectedIndex');
            var cls = clset[sel_idx];
            if(cls && cls_special_case(cls, special_cases)) {}
            else if(cls) {
                $('.blank_suit div.' + cls).css('visibility', 'visible');
            }
        }
        suit_fason_special_case(special_cases['suit_fason']);
        delete special_cases['suit_fason'];
    });
    // trigger designer redraw on page load
    $('#design_form select:first').trigger('change');

    function cls_special_case(cls, special_cases) {
        for (var sc in special_cases) {
            if(cls.indexOf(sc) == 0) {
                // example: 
                // sc: 'suit_fason'
                // cls: 'suit_fason#1pilo'
                // special_cases[sc]: suit_fason#0_#1_#2 ---> suit_fason#0_pilo_#2
                var re1 = cls.substr(sc.length, 2)
                var re2 = cls.substr(sc.length + 2)
                special_cases[sc] = special_cases[sc].replace(re1, re2);
                return true;
            }
        }
        return false;
    }

    function suit_fason_special_case(cls) {
        if($('#d_opt_0').prop('selectedIndex') == 2) {
            cls = cls.replace('#0', '3');
        }
        else {
            cls = cls.replace('#0', '21');
        }
        if(cls.indexOf('#') == -1) {
            $('.blank_suit div.' + cls).css('visibility', 'visible');
        }
    }

});
