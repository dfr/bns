// style select
(function($) {

    function style_selects() {
	$('.field1').click(function() {
	    $('#field1').show();
		$('#field2').hide();
		$('#field3').hide();
		$(this).addClass('u_active_link');
		$('.field2').removeClass('u_active_link');
		$('.field3').removeClass('u_active_link');
		return true;
  	});
	
	$('.field2').click(function() {
	    $('#field1').hide();
		$('#field2').show();
		$('#field3').hide();
		$(this).addClass('u_active_link');
		$('.field1').removeClass('u_active_link');
		$('.field3').removeClass('u_active_link');
		return true;
  	});
	
	$('.field3').click(function() {
	    $('#field1').hide();
		$('#field2').hide();
		$('#field3').show();
		$(this).addClass('u_active_link');
		$('.field1').removeClass('u_active_link');
		$('.field2').removeClass('u_active_link');
		return true;
  	});
    }

    function select_fabric() {
    }

    $(document).ready(function(){
        style_selects();


    });
})(jQuery);
