// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery-ui-1.8.20.custom.min
//= require jquery.ui.datepicker-ru
//= require jquery_ujs
//= require cusel-min-2.4.1
//= require jquery.fullscreenr
//= require interface
//= require table
//= require jquery.easing.1.3
//= require jquery.mousewheel.min
//= require jquery.mCustomScrollbar
//= require jquery.fancybox
//= require jquery.ba-resize
//= require_tree ./site
//= require_tree ./admin
//= require markitup
//= require markitup-html
