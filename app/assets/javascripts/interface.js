// hidden num 5 and 15 separator in menu
$(document).ready(function() {
	$('.nav_menu li').eq(5).css('border-right', 'none');
	$('.nav_menu li').eq(15).css('border-right', 'none');
	$('.auth li').eq(0).css('border-right', 'none');
	$('.auth_form').prev().css('padding', '15px 12px 15px 15px');
	$('.auth li').eq(1).css('border-right', 'none');
//reg form	
	$('.reg_form label').eq(0).addClass('first_reg_label');
	$('.reg_form label').eq(2).addClass('second_reg_label');
	$('.reg_form input').eq(0).addClass('reg_input');
	$('.reg_form input').eq(2).addClass('reg_input');
	
});

//fullscreen background
$(document).ready(function() {
// You need to specify the size of your background image here (could be done automatically by some PHP code)
var FullscreenrOptions = { width: 1680, height: 1050, bgID: '#bgimg' };
// This will activate the full screen background!
jQuery.fn.fullscreenr(FullscreenrOptions);
});

// show auth form
$(document).ready(function() {
	$('.auth li:first-child').live('mouseover', function(){
		$('.auth li').eq(0).css('cursor', 'pointer');
		$('.auth li').eq(0).addClass('active_account');
	}).live('mouseout', function(){
		$('.auth li').eq(0).removeClass('active_account');
	});
	
	$('.auth li a').eq(0).click(function() {
	    $('.auth_form').toggle();
		$('.auth li').eq(0).toggleClass('active_form');
		$('#email').focus();
		//$('.m_close').toggleClass('visible');
		$('.mailer_form').hide();
		$('li.mailer_btn').removeClass('active_form');
		$('.contacts_form').hide();
		$('li.contacts_btn').removeClass('active_form');
		return false;
 	});
	
	$('li.mailer_btn').click(function() {
	    $('.mailer_form').toggle();
		$('li.mailer_btn').toggleClass('active_form');
		$('#mailer_email').focus();
		return false;
  	});
	
	$('li.contacts_btn').click(function() {
	    $('.contacts_form').toggle();
		$('li.contacts_btn').toggleClass('active_form');
		return false;
  	});
	
});


// show auth form
$(document).ready(function() {
// show footer panel
//mailer_btn
	$('.contacts li.mailer_btn').live('mouseover', function(){
		$(this).eq(0).css('cursor', 'pointer');
		$(this).eq(0).addClass('active_account');
	}).live('mouseout', function(){
		$(this).eq(0).removeClass('active_account');
	});
//contacts_btn
	$('.contacts li.contacts_btn').live('mouseover', function(){
		$(this).eq(0).css('cursor', 'pointer');
		$(this).eq(0).addClass('active_account');
	}).live('mouseout', function(){
		$(this).eq(0).removeClass('active_account');
	});
	
});


// hide auth form on click another page
jQuery(document).ready(function() {
//click to page
	$('.page').click(function() {
	    $('.auth_form, .mailer_form, .contacts_form').hide();
		$('.auth li').eq(0).removeClass('active_form');
		$('li.mailer_btn').removeClass('active_form');
		$('li.contacts_btn').removeClass('active_form');
		//$('.m_close').removeClass('visible');
  	});
	
//click to mailer_btn	
	$('li.mailer_btn').click(function() {
	    $('.auth_form').hide();
		$('.auth li').eq(0).removeClass('active_form');
		$('.contacts_form').hide();
		$('li.contacts_btn').removeClass('active_form');
		//$('.m_close').removeClass('visible');
  	});
//click to contacts_btn
	$('li.contacts_btn').click(function() {
	    $('.mailer_form').hide();
		$('li.mailer_btn').removeClass('active_form');
		$('.auth_form').hide();
		$('.auth li').eq(0).removeClass('active_form');
		//$('.m_close').removeClass('visible');
  	});	

	
	$('.auth_form .form_inner, .mailer_form, .contacts_form, .auth_form .m_menu').click(function(event){
    // Клик оказался в пределах?
    // Распространению — нет!
    event.stopPropagation();
 	});
	
});

// m_menu correct
$(document).ready(function() {
	$('.m_menu a').eq(3).addClass('lm_menu');
	$('.m_menu a:first-child').css('padding-top', '0px');
});


// active main menu
$(document).ready(function() {
	$('#buy').click(function() {
		$('#buy').parent().toggleClass('current_menu');
	    $('.nav_menu.submenu').toggle();
  	});
	
});

// content scroller

$(window).load(function() {
    $('.page').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0}, 200);
    mCustomScrollbars();
    $(".content").resize(function(e){ $("#mcs3_container").mCustomScrollbar("vertical",0,"easeOutCirc",1.05,"auto","yes","no",0); });
});

function mCustomScrollbars(){
	/* 
	malihu custom scrollbar function parameters: 
	1) scroll type (values: "vertical" or "horizontal")
	2) scroll easing amount (0 for no easing) 
	3) scroll easing type 
	4) extra bottom scrolling space for vertical scroll type only (minimum value: 1)
	5) scrollbar height/width adjustment (values: "auto" or "fixed")
	6) mouse-wheel support (values: "yes" or "no")
	7) scrolling via buttons support (values: "yes" or "no")
	8) buttons scrolling speed (values: 1-20, 1 being the slowest)
	*/
	$("#mcs3_container").mCustomScrollbar("vertical",0,"easeOutCirc",1.05,"auto","yes","no",0);
}

/* function to fix the -10000 pixel limit of jquery.animate */
$.fx.prototype.cur = function(){
    if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
      return this.elem[ this.prop ];
    }
    var r = parseFloat( jQuery.css( this.elem, this.prop ) );
    return typeof r == 'undefined' ? 0 : r;
}

// fansy box
$(document).ready(function() {
//sizing table
	$('.sizing').click(function() {
		$('body').addClass('overflow_auto')
  	});
	
	$('.sizing').fancybox({
		'fixed': true,
		'fitToView': false
	});
	
//registration	
	$('.reg').fancybox({
		'fixed': true,
		'fitToView': false,
		'afterLoad': function () {
                    $('#registration .hint').hide();
                    $('#registration .reg_block').show();
                },
		minHeight : 50
	});

//Forgot your password?
	$('.forgot').fancybox({
		'fixed': true,
		'fitToView': false,
		minHeight : 50
	});

//Изменить цвет декора в конструкторе
	$('.change_decor').fancybox({
		'fixed': true,
		'fitToView': false,
		minHeight : 50
	});
	
});

//month scroll
/*
$(document).ready(function() {
	$(".m_arrow_right").click(function(){
		$('.table1').hide();
		$('.table2').show();
	});
	
	$(".m_arrow_left").click(function(){
		$('.table1').show();
		$('.table2').hide();
	});
});

*/

//style user list table
$(document).ready(function() {
	$('.user_list table tr td:nth-child(2)').css('color', '#000');
	$('.user_list table tr td:first-child').addClass('user_cel');
	$('.user_list table tr td:last-child').addClass('arrow_cel');
	
	//table line
	$('.user_list table tr').live('mouseover', function(){
		$(this).addClass('active_hover');
		$('.active_hover td:first-child').addClass('first_round');
		$('.active_hover td:last-child').addClass('last_round');
		
	}).live('mouseout', function(){
		$(this).removeClass('active_hover');
		$('.user_list table tr td').removeClass('first_round');
		$('.user_list table tr td').removeClass('last_round');
	});
	
	//table width cel
	$('.user_list table tr th:nth-child(1)').css('width', '180px');
	$('.user_list table tr th:nth-child(2)').css('width', '200px');
	$('.user_list table tr th:nth-child(3)').css('width', '90px');
	$('.user_list table tr th:nth-child(4)').css('width', '174px');
	$('.user_list table tr th:nth-child(5)').css('width', '90px');
	$('.user_list table tr th:nth-child(6)').css('width', '170px');
	$('.user_list table tr th:nth-child(7)').css('width', '16px');
	

        jQuery( function($) {
            $('tbody').on('click', 'tr[data-href]', function() {
                window.location = $(this).attr('data-href');
            }).find('a').hover( function() {
                $(this).parents('tr').unbind('click');
            }, function() {
                $(this).parents('tr').click( function() {
                    window.location = $(this).attr('data-href');
                });
            });
        });
	
});






// style select
jQuery(document).ready(function(){

	var params = {
		changedEl: ".lineForm select",
		visRows: 5,
		scrollArrows: false
	}

	cuSel(params);
	
	var params = {
		changedEl: "#user_sort",
		scrollArrows: false
	}

	cuSel(params);

	var params1 = {changedEl: "#options_item1",scrollArrows: false}
	cuSel(params1);
	var params2 = {changedEl: "#options_item2",scrollArrows: false}
	cuSel(params2);
	var params3 = {changedEl: "#options_item3",scrollArrows: false}
	cuSel(params3);
	var params4 = {changedEl: "#options_item4",scrollArrows: false}
	cuSel(params4);
	var params5 = {changedEl: "#options_item5",scrollArrows: false}
	cuSel(params5);
	var params6 = {changedEl: "#options_item6",scrollArrows: false}
	cuSel(params6);
	var params7 = {changedEl: "#options_item7",scrollArrows: false}
	cuSel(params7);
	var params8 = {changedEl: "#options_item8",scrollArrows: false}
	cuSel(params8);
	var params9 = {changedEl: "#options_item8",scrollArrows: false}
	cuSel(params9);
});

//u_table sizer
jQuery(document).ready(function(){
	$('.u_table tr th:nth-child(1)').css('width', '240px');
	$('.u_table tr th:nth-child(2)').css('width', '90px');
	$('.u_table tr th:nth-child(3)').css('width', '90px');
	$('.u_table tr th:nth-child(4)').css('width', '55px');
	
	//zakaz
	$('.zakaz tr th:nth-child(1)').css('width', '105px');
	$('.zakaz tr th:nth-child(2)').css('width', '95px');
	$('.zakaz tr th:nth-child(3)').css('width', '200px');
	$('.zakaz tr th:nth-child(4)').css('width', '90px');
	
	//reviews
	$('.reviews tr th:nth-child(1)').css('width', '105px');
	$('.reviews tr th:nth-child(2)').css('width', '95px');
	$('.reviews tr th:nth-child(3)').css('width', '110px');
	$('.reviews tr th:nth-child(4)').css('width', '90px');
	$('.reviews tr th:nth-child(5)').css('width', '90px');
	
	
	
	
	$('.u_table tr td:last-child a').addClass('u_edit');
	$('.u_more div:last-child').css('border-bottom', '0px');
	$('.u_table tr.hidden td').css('padding', '0px 1px');
	
	// open more options in size
	$('.u_size').click(function() {
		//$(this).css('background-position', '0px 0px');
		$(this).parents('tr').next().toggle();
	});
	// open img in size
//	$('.u_status .u_size').click(function() {
		//$(this).css('background-position', '0px 0px');
//		$(this).prev().toggle();
//	});
	
	
});

//add standart scroller
jQuery(document).ready(function(){
	$('.clear_container').parents('body').css('overflow', 'auto');
});


//hover_content
jQuery(document).ready(function(){
	$('.hover_content1, .hover_content2, .hover_content3, .hover_content4, .hover_content5, .hover_content6, .hover_content7, .hover_content8, .hover_content9, .hover_content10').hide();

	$('.l1').click(function() {
		$(this).next().slideToggle('slow');
	//	$(this).next().toggle();
		$(this).toggleClass('active_list');
		$('.hover_content2, .hover_content3, .hover_content4, .hover_content5, .hover_content6, .hover_content8, .hover_content9, .hover_content10').slideUp('slow');
		$('.l2, .l3, .l4, .l5, .l6').removeClass('active_list');
		return false;
  	});

  	$('.l2').click(function() {
		$(this).next().slideToggle('slow');
		$(this).toggleClass('active_list');
		$('.hover_content1, .hover_content3, .hover_content4, .hover_content5, .hover_content6, .hover_content7, .hover_content9, .hover_content10').slideUp('slow');
		$('.l1, .l3, .l4, .l5, .l6').removeClass('active_list');
		return false;
  	});

  	$('.l3').click(function() {
		$(this).next().slideToggle('slow');
		$(this).toggleClass('active_list');
		$('.hover_content1, .hover_content2, .hover_content4, .hover_content5, .hover_content6, .hover_content7, .hover_content8, .hover_content10').slideUp('slow');
		$('.l1, .l2, .l4, .l5, .l6').removeClass('active_list');
		return false;
  	});

  	$('.l4').click(function() {
		$(this).next().slideToggle('slow');
		$(this).toggleClass('active_list');
		$('.hover_content1, .hover_content3, .hover_content2, .hover_content5, .hover_content6, .hover_content7, .hover_content8, .hover_content9').slideUp('slow');
		$('.l1, .l2, .l3, .l5, .l6').removeClass('active_list');
		return false;
  	});

  	$('.l5').click(function() {
		$(this).next().slideToggle('slow');
		$(this).toggleClass('active_list');
		$('.hover_content1, .hover_content3, .hover_content4, .hover_content2, .hover_content6').slideUp('slow');
		$('.l1, .l2, .l3, .l4, .l6').removeClass('active_list');
		return false;
  	});

  	$('.l6').click(function() {
		$(this).next().slideToggle('slow');
		$(this).toggleClass('active_list');
		$('.hover_content1, .hover_content3, .hover_content4, .hover_content5, .hover_content2').slideUp('slow');
		$('.l1, .l2, .l3, .l4, .l5').removeClass('active_list');
		return false;
  	});



	
});


// click for exit
$(document).ready(function() {
	$('.exit').click(function() {
		$('.exit_btn').click();
  	});
	
});


//datapicker
jQuery(document).ready(function(){
$(function() {
		$( "#datepicker" ).datepicker($.datepicker.regional[ "ru" ]);
		$('#datepicker').datepicker('option', 'changeYear', false);
		
	});
});

// calendar time clients
jQuery(document).ready(function(){
    // свободно
    $('.select_time .u_options a:nth-child(1):not(.u_active_link)').live('click', function () {
        var top = $(this).closest('.select_time');
        top.find('.time_client, .input_client').hide();
        $(this).addClass('u_active_link');
        $(this).next().removeClass('u_active_link');
    });
    // забронировано
    $('.select_time .u_options a:nth-child(2):not(.u_active_link)').live('click', function () {
        var top = $(this).closest('.select_time');
        top.find('.input_client input[type=text]').val('');
        top.find('.input_client').show();
        $(this).addClass('u_active_link');
        $(this).prev().removeClass('u_active_link');
    });
    // set user_name
    $('.select_time form').live('ajax:success', function(event, data) {
        var top = $(this).closest('.select_time');
        if(data.status == 'ok') {
            top.find('.input_client').hide();
            top.find('.time_client a').text(data.user_name);
            top.find('.time_client').show();
        }
    });
});


function isAppleDevice(){
return (
(navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
(navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
(navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
);
}
jQuery(document).ready(function() {
if (navigator.userAgent.toLowerCase().indexOf("ipad") > -1){
	$('.header_container').css('width', '1026px');
	$('.page').css('min-width', '1026px');
	$('.nav_menu a, .auth a, .contacts a').css('padding', '0 10px 0 5px');
	$('.administrator .nav_menu a, .administrator .auth a, .administrator .contacts a').css('padding', '0 0px 0 5px');
	
}

if (navigator.userAgent.toLowerCase().indexOf("iphone") > -1){
	$('.header_container').css('width', '1026px');
	$('.page').css('min-width', '1026px');
	$('.nav_menu a, .auth a, .contacts a').css('padding', '0 10px 0 5px');
	$('.administrator .nav_menu a, .administrator .auth a, .administrator .contacts a').css('padding', '0 0px 0 5px');
	
}

if (navigator.userAgent.toLowerCase().indexOf("ipod") > -1){
	$('.header_container').css('width', '1026px');
	$('.page').css('min-width', '1026px');
	$('.nav_menu a, .auth a, .contacts a').css('padding', '0 10px 0 5px');
	$('.administrator .nav_menu a, .administrator .auth a, .administrator .contacts a').css('padding', '0 0px 0 5px');
	
}
//alert(navigator.platform)
}); 





//hover_content_design
jQuery(document).ready(function(){
	$('.submenu_design a').click(function() {
		$(this).next().slideToggle('slow');
	//	$(this).next().toggle();
	//	$(this).toggleClass('active_list');
	//	$('.hover_content2, .hover_content3, .hover_content4, .hover_content5, .hover_content6, .hover_content8, .hover_content9, .hover_content10').slideUp('slow');
	//	$('.l2, .l3, .l4, .l5, .l6').removeClass('active_list');
		return false;
  	});
 });


// click for exit
$(document).ready(function() {
	$('.product').click(function() {
		$(this).children('.product_hover').toggle();
		$(this).children('.product_img').toggleClass('pro_active');
  	});
  	
	
});


