//sizing_table table settings
$(document).ready(function() {
	$('.sizing_table tr td:first-child').addClass('m_first');
	$('.sizing_table tr td:last-child').addClass('m_last');
	
	$('.sizing_table tr th:first-child').addClass('m_first');
	$('.sizing_table tr th:last-child').addClass('m_last');
});


//sizing_table mouseover mouseout 
$(document).ready(function() {
	$('.sizing_table tr td').live('mouseover', function(){
		$(this).addClass('sizing_hover');
		
	}).live('mouseout', function(){
		$(this).removeClass('sizing_hover');
	});
});



/* Table 1
---------------------------------------- */
$(document).ready(function() {

//1 строка
	$(".table1 tr").eq(1).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(1).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(1) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active1');
		$(this).siblings().removeClass('sizing_active1');
		$('.sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
// 2 строка	
	$(".table1 tr").eq(2).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(2).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(2) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active2');
		$(this).siblings().removeClass('sizing_active2');
		$('.sizing_active1, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
// 3 строка	
	$(".table1 tr").eq(3).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(3).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(3) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active3');
		$(this).siblings().removeClass('sizing_active3');
		$('.sizing_active1, .sizing_active2, .sizing_active4, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});

	
// 4 строка	
	$(".table1 tr").eq(4).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(4).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(4) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active4');
		$(this).siblings().removeClass('sizing_active4');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active5, .sizing_active6, .sizing_active7').removeClass();
	});
	

// 5 строка	
	$(".table1 tr").eq(5).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(5).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(5) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active5');
		$(this).siblings().removeClass('sizing_active5');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active6, .sizing_active7').removeClass();
	});
	
// 6 строка	
	$(".table1 tr").eq(6).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(6).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(6) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active6');
		$(this).siblings().removeClass('sizing_active6');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});

// 6 строка	
	$(".table1 tr").eq(7).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
	$(".table1 tr").eq(7).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(7) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active7');
		$(this).siblings().removeClass('sizing_active7');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active6').removeClass();
	});
	
// 7 строка	
	$(".table1 tr").eq(8).find('td').eq(1).click(function(){
		var time = $("table tr:eq(0) th:eq(1)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(2).click(function(){
		var time = $("table tr:eq(0) th:eq(2)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(3).click(function(){
		var time = $("table tr:eq(0) th:eq(3)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(4).click(function(){
		var time = $("table tr:eq(0) th:eq(4)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(5).click(function(){
		var time = $("table tr:eq(0) th:eq(5)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(6).click(function(){
		var time = $("table tr:eq(0) th:eq(6)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(7).click(function(){
		var time = $("table tr:eq(0) th:eq(7)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(8).click(function(){
		var time = $("table tr:eq(0) th:eq(8)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});
	
	$(".table1 tr").eq(8).find('td').eq(9).click(function(){
		var time = $("table tr:eq(0) th:eq(9)").text();
		var data = $("table tr:eq(8) td:eq(0)").text()
		$('.times span').text(time + ', ' + data);
		$(this).addClass('sizing_active8');
		$(this).siblings().removeClass('sizing_active8');
		$('.sizing_active1, .sizing_active2, .sizing_active3, .sizing_active4, .sizing_active5, .sizing_active7').removeClass();
	});

	
//currents day
	$(".current_day").click(function(){
		$('.times span').text('Время зарезервировано');
	});
});