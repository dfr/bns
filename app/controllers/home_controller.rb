class HomeController < ApplicationController
  def index
  end

  def about
  end

  def gift_certificates
  end

  def faq
  end

  def quality_guarantee
  end

  def material
  end

  def content
    path = [params[:p1], params[:p2]] * '-'
    @article = Article.find_by_path(path)
    @article ||= Article.new(:path => path, :content => 'Coming soon..')
  end
end
