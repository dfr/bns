
class ConfirmationsController < Devise::ConfirmationsController

  def show
    flash[:confirmed] = 1
    super
  end
end
