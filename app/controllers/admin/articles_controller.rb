# encoding: utf-8
class Admin::ArticlesController < ApplicationController
  layout "admin"

  def index
    @articles = Article.all
  end

  # GET /admin/articles/1/edit
  def edit
    @article = Article.find_by_path(params[:id]) || Article.new(:path => params[:id])
  end

  # POST /admin/articles
  # POST /admin/articles.json
  def create
    @article = Article.new(params[:article])

    respond_to do |format|
      if @article.save
        format.html { redirect_to admin_articles_path, notice: 'Текст успешно добавлен.' }
        format.json { render json: @article, status: :created, location: @article }
      else
        format.html { render action: "new" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/articles/1
  # PUT /admin/articles/1.json
  def update
    @article = Article.find(params[:id])


    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to admin_articles_path, notice: 'Текст успешно обновлен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  def preview
    render :layout => 'preview'
  end

end
