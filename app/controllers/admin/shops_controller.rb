# encoding: utf-8

class Admin::ShopsController < ApplicationController
  #before_filter :authenticate_user!
  layout "admin"

  def day_measures
    #@shop = Shop.find(params[:id])
    #@shop.day_measures
    @date = params[:date].to_date
    mysql_date = @date.strftime('%Y-%m-%d')
    measures = ShopMeasure.all :conditions => [ 'shop_id = ? AND DATE(at) = ?', params[:id], mysql_date ]
    by_hour = Hash[ measures.map { |obj| [obj.at.hour, obj] } ]
    @measures = []
    ShopMeasure.transaction do
      (10..21).each do |h|
        measure = by_hour[h]
        if !measure
          at = @date.to_datetime.change({:hour => h , :min => 0 , :sec => 0 })
          measure = ShopMeasure.new(:at => at, :isbusy => true, :shop_id => params[:id])
          measure.save!(:validate => false)
        end
        @measures.push measure
      end
    end
    render :layout => false
  end

  def index
    @shops = Shop.all
  end

  # GET /admin/shops/new
  # GET /admin/shops/new.json
  def new
    @shop = Shop.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @shop }
    end
  end

  # GET /admin/shops/1/edit
  def edit
    @shop = Shop.find(params[:id])
  end

  # POST /admin/shops
  # POST /admin/shops.json
  def create
    @shop = Shop.new(params[:shop])

    respond_to do |format|
      if @shop.save
        format.html { redirect_to admin_shops_path, notice: 'Магазин успешно добавлен.' }
        format.json { render json: @shop, status: :created, location: @shop }
      else
        format.html { render action: "new" }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/shops/1
  # PUT /admin/shops/1.json
  def update
    @shop = Shop.find(params[:id])


    respond_to do |format|
      if @shop.update_attributes(params[:shop])
        #if to_filename = upload_file(params[:shop][:file], @shop.id)
        #  @shop.update_column(:mapfilename, to_filename)
        #end
        format.html { redirect_to admin_shops_path, notice: 'Магазин успешно обновлен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @shop.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/shops/1
  # DELETE /admin/shops/1.json
  def destroy
    @shop = Shop.find(params[:id])
    @shop.destroy

    respond_to do |format|
      format.html { redirect_to admin_shops_url, notice: 'Магазин успешно удален.' }
      format.json { head :no_content }
    end
  end

end
