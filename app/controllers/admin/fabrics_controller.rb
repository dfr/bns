# encoding: utf-8
class Admin::FabricsController < ApplicationController
  layout "admin"

  def index
    @fabrics = [params[:kind], params[:element]].reduce(Fabric.scoped) { |fabric, opt|
      opt.nil? ? fabric : fabric.send(opt)
    }
  end

  # GET /admin/fabrics/new
  # GET /admin/fabrics/new.json
  def new
    checked = [params[:kind], params[:element]].reduce({}) { |h,opt| opt.nil? ? h : h.merge(opt => 1) }
    @fabric = Fabric.new(checked)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fabric }
    end
  end

  # GET /admin/fabrics/1/edit
  def edit
    @fabric = Fabric.find(params[:id])
  end

  # POST /admin/fabrics
  # POST /admin/fabrics.json
  def create
    @fabric = Fabric.new(params[:fabric])

    respond_to do |format|
      if @fabric.save
        format.html { redirect_to admin_fabrics_path, notice: 'Материал успешно добавлен.' }
        format.json { render json: @fabric, status: :created, location: @fabric }
      else
        format.html { render action: "new" }
        format.json { render json: @fabric.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/fabrics/1
  # PUT /admin/fabrics/1.json
  def update
    @fabric = Fabric.find(params[:id])


    respond_to do |format|
      if @fabric.update_attributes(params[:fabric])
        #if to_filename = upload_file(params[:fabric][:file], @fabric.id)
        #  @fabric.update_column(:mapfilename, to_filename)
        #end
        format.html { redirect_to admin_fabrics_path, notice: 'Материал успешно обновлен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fabric.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/fabrics/1
  # DELETE /admin/fabrics/1.json
  def destroy
    @fabric = Fabric.find(params[:id])
    @fabric.destroy

    respond_to do |format|
      format.html { redirect_to admin_fabrics_url, notice: 'Материал успешно удален.' }
      format.json { head :no_content }
    end
  end
end
