# encoding: utf-8

class Admin::ShopMeasureController < ApplicationController
  #before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token
  
  def set_busy
    @measure = ShopMeasure.find(params[:id])
    @measure.isbusy = params[:busy]
    unless @measure.isbusy
      @measure.user_name = nil
      @measure.user_id = nil
    end
    @measure.save!
    render json: { 'status' => 'ok' }
  end

  def set_username
    unless params[:user_name].blank?
      @measure = ShopMeasure.find(params[:id])
      @measure.user_name = params[:user_name]
      @measure.save!
      status = 'ok'
    else
      status = 'blankval'
    end
    render json: { 'status' => status, 'user_name' => params[:user_name] }
  end
end
