# encoding: utf-8
class Admin::AccessoriesController < ApplicationController
  layout "admin"

  def index
    @accessories = Accessory.all
  end

  # GET /admin/accessories/new
  # GET /admin/accessories/new.json
  def new
    @accessory = Accessory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @accessory }
    end
  end

  # GET /admin/accessories/1/edit
  def edit
    @accessory = Accessory.find(params[:id])
  end

  # POST /admin/accessories
  # POST /admin/accessories.json
  def create
    @accessory = Accessory.new(params[:accessory])

    respond_to do |format|
      if @accessory.save
        format.html { redirect_to admin_accessories_path, notice: 'Аксессуар успешно добавлен.' }
        format.json { render json: @accessory, status: :created, location: @accessory }
      else
        format.html { render action: "new" }
        format.json { render json: @accessory.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/accessories/1
  # PUT /admin/accessories/1.json
  def update
    @accessory = Accessory.find(params[:id])


    respond_to do |format|
      if @accessory.update_attributes(params[:accessory])
        format.html { redirect_to admin_accessories_path, notice: 'Аксессуар успешно обновлен.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @accessory.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/accessories/1
  # DELETE /admin/accessories/1.json
  def destroy
    @accessory = Accessory.find(params[:id])
    @accessory.destroy

    respond_to do |format|
      format.html { redirect_to admin_accessories_url, notice: 'Аксессуар успешно удален.' }
      format.json { head :no_content }
    end
  end
end
