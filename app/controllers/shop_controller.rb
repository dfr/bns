
class ShopController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:book_hour]
  #before_filter :authenticate_user!, :only => [:book_hour]

  def index
    # TODO: implement shop selector
    @shop = Shop.first || Shop.new(title: "No shops found in database")
  end

  def book_hour
    at = DateTime.strptime("#{params[:date]} #{params[:hour]}", '%Y-%m-%d %H')
    measure = ShopMeasure.where(:at => at, :shop_id => params[:id]).first_or_initialize
    measure.update_attributes(params[:book])
    measure.isbusy = true
    if user_signed_in?
      measure.user_id = current_user.id
    end
    if measure.save
      render json: { :status => 'ok', :id => "td_#{at.to_date}_#{at.hour}" }
    else
      render json: measure.errors, status: :unprocessable_entity
    end
  end

  def sizing_table
    @shop_id = params[:id].to_i
    @date_start = Date.strptime("#{params[:year]} #{params[:week]}", '%Y %W')
    @date_end = @date_start.end_of_week
    measures = ShopMeasure.where(
      [ 'shop_id = ? AND DATE(at) >= ? AND DATE(at) <= ?', @shop_id ] +
      [ @date_start, @date_end ].map { |d| d.strftime('%Y-%m-%d') })
      .order('at')
      .limit(120)
    @by_dayhour = Hash[ measures.map { |obj| [obj.at.wday.to_s + '_' + obj.at.hour.to_s, obj] } ]
    @time_range = 10..21
    render :layout => false
  end
end
