
class Buy::JacketController < Buy::BaseController

  def steps
    [nil,
     [:choose_fabric, :cloth],
     [:choose_fabric, :lining],
     [:design, :jacket]]
  end

end
