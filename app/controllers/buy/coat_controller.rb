
class Buy::CoatController < Buy::BaseController

  def steps
    [nil,
     [:choose_fabric, :cloth],
     [:choose_fabric, :lining],
     [:design, :coat]]
  end


end
