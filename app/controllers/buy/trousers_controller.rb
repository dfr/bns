
class Buy::TrousersController < Buy::BaseController

  def steps
    [nil,
     [:choose_fabric, :cloth],
     [:design, :trousers]]
  end

end
