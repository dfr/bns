
class Buy::SuitController < Buy::BaseController

  def steps
    [nil,
     [:choose_fabric, :cloth],
     [:choose_fabric, :lining],
     [:design, :jacket],
     [:design, :trousers]]
  end

end
