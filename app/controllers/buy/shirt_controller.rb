
class Buy::ShirtController < Buy::BaseController

  def steps
    [nil,
     [:choose_fabric, :coat],
     [:design, :shirt]]
  end

end
