
class Buy::BaseController < ApplicationController

  SES_KEY = 'buy'
  before_filter :setup_wizard

  def index
    if(@step_kind == :choose_fabric)
      list_fabrics
    elsif(@step_kind == :design)
      design
    end
  end

  def list_fabrics
    @grouped_items = Fabric::Grouped.new(params[:sort], Fabric.send(@step_subj))
    render 'index'
  end

  def design
    render 'design'
  end

  def select_fabric
    session.deep_merge!({SES_KEY => { :fabs => { @step_subj => params[:id].to_i } }})
    redirect_to url_for(:action => :index, :step => @step + 1)
  end

  def select_design
    session.deep_merge!({SES_KEY => { :designs => params[:design] }})
    #puts "*#{session.inspect}*"
    # TODO redirect to cart ?
    redirect_to url_for(:action => :index, :step => @step + 1)
  end

  private

  def setup_wizard
    @step = params.fetch(:step, 1).to_i
    @fabs = session.fetch(SES_KEY, {}).fetch(:fabs, {})
    @designs = session.fetch(SES_KEY, {}).fetch(:designs, {})
    @title = params[:controller].split('/')[-1]
    check_steps_until(@step)
    @step_kind, @step_subj = steps[@step]
    @n_fabric_steps = steps.select { |kind, subj| kind == :choose_fabric }.size
    @selected_fabrics = @fabs.values.map { |id| Fabric.find(id) }
  end

  def check_steps_until(current_step)
    (1..current_step-1).each do |step|
      skind, subj = steps[step]
      if skind == :choose_fabric
        if @fabs.fetch(subj, nil).nil?
          logger.debug("!!! fabric for #{subj} not choosen")
          return redirect_to url_for(:step => step)
        end
      elsif skind == :design
        if @designs.fetch(subj, nil).nil?
          logger.debug("!!! design for #{subj} not done")
          return redirect_to url_for(:step => step)
        end
      else
        logger.debug "unknown step kind! #{skind}"
      end
    end
  end

end

