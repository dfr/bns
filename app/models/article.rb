# encoding: utf-8
class Article < ActiveRecord::Base
  # разделы сайа (! каждый пункт должен содержать тире '-' !)
  PATHS=[
    ['shops-delivery', 'Магазины - Наша доставка'],
    ['quality-process', 'Качество - Описание процесса'],
    ['quality-guarantee', 'Качество - Гарантии качества'],
    ['quality-fabrics', 'Качество - Ткани'],
    ['quality-faq', 'Качество - F.A.Q.'],
    ['gifts-prices', 'Подарки & скидки - Цены'],
    ['gifts-certificates', 'Подарки & скидки - Подарочные сертификаты'],
    ['gifts-discounts', 'Подарки & скидки - Скидки']]

    after_initialize :set_title

    attr_accessible :content, :path, :title, :publish

    def category
      Hash[PATHS].fetch(path, nil)
    end

    # return subset of PATHS for view 'gifts-prices' => [['gifts-prices',..], ['gifts-..',..],..]
    def submenu
      p1, = path.split '-'
      PATHS.select { |p,title| p.starts_with?(p1) }
    end

    private

    def set_title
      self.title = category if self.title.nil?
    end


end
