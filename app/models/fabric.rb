# encoding: utf-8

class Fabric < ActiveRecord::Base
  CLOTH_KINDS =  {
    1 => :suit,
    2 => :jacket,
    3 => :trousers,
    4 => :shirt,
    5 => :coat
  }
  CLOTH_ELEMENTS = {
    1 => :cloth,
    2 => :lining
  }

  attr_accessible :color, :description, :element, :kind, :price, :publish, :style, :texture, :title,
    :material, :density, :collection,
    :photo, :photo_cache, :remove_photo, *CLOTH_ELEMENTS.values, *CLOTH_KINDS.values

  validates :title, :presence => true, :uniqueness => true
  validates :price, :numericality => { :only_integer => true }, :allow_blank => true
  validates :element, :numericality => { :greater_than => 0, :message => 'должен быть выбран' }

  scope :published, where(:publish => true)

  include FlagShihTzu
  has_flags CLOTH_KINDS.merge(column: 'kind')
  has_flags CLOTH_ELEMENTS.merge(column: 'element')

  mount_uploader :photo, FabricUploader

  class Grouped
    attr_reader :groups, :sort_param, :items
    def initialize(param, model)
      @sort_param = param =~ /^[a-z_]+$/ ? param : 'color'
      if @sort_param == 'price'
        @groups = []
      else
        @groups = model.select(@sort_param).group(@sort_param)
          .map { |i| i[@sort_param] }
          .sort{ |a,b| a.to_s.empty? ? 1 : a <=> b }
      end
      @items = model.order("#{@sort_param}, title")
    end

    def by_group(group)
      if @sort_param == 'price'
        @items
      else
        @items.find_all { |i| i[@sort_param] == group }
      end
    end

  end

end
