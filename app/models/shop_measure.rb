class ShopMeasure < ActiveRecord::Base
  belongs_to :shop
  belongs_to :user
  attr_accessible :at, :isbusy, :shop_id, :phone, :email, :user_name

  validates :user_name, :phone, :presence => true
end
