
class Shop < ActiveRecord::Base
  attr_accessible :address, :time, :phone, :title, :description, :map, :remove_map, :map_cache
  validates :title, :presence => true, :uniqueness => true
  has_many :shop_measures, :dependent => :delete_all
  mount_uploader :map, MapUploader
end
