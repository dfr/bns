class Accessory < ActiveRecord::Base
  attr_accessible :description, :material, :price, :publish, :title, :category,
    :photo, :photo_cache, :remove_photo

  validates :title, :presence => true, :uniqueness => true
  validates :price, :numericality => { :only_integer => true }, :allow_blank => true
end
