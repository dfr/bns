class AddPhoneEmailToShopMeasures < ActiveRecord::Migration
  def change
    add_column :shop_measures, :phone, :string
    add_column :shop_measures, :email, :string
  end
end
