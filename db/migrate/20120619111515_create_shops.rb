class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :title, :null => false
      t.string :address
      t.text :time
      t.string :phone

      t.timestamps
    end
    add_index :shops, :title, :unique => true
  end
end
