class CreateFabrics < ActiveRecord::Migration
  def change
    create_table :fabrics do |t|
      t.string :title, :null => false
      t.string :color
      t.integer :price
      t.text :description
      t.boolean :publish, :null => false, :default => 1
      t.string :style
      t.string :texture
      t.string :photo
      t.integer :kind, :null => false, :default => 0
      t.integer :element, :null => false, :default => 0

      t.timestamps
    end
  end
end
