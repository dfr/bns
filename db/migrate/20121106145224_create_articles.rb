class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.text :path, :null => false, :unique => true
      t.boolean :publish, :null => false, :default => 1

      t.timestamps
    end
  end
end
