class AddDescriptionMapfilenameToShop < ActiveRecord::Migration
  def change
    add_column :shops, :description, :text
    add_column :shops, :map, :string
  end
end
