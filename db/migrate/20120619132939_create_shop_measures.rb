class CreateShopMeasures < ActiveRecord::Migration
  def change
    create_table :shop_measures do |t|
      t.references :shop, :null => false
      t.references :user
      t.datetime :at, :null => false
      t.boolean :isbusy, :default => false

      t.timestamps
    end
    add_index :shop_measures, :shop_id
    add_index :shop_measures, :user_id
  end
end
