class AddMaterialDensityCollectionToFabrics < ActiveRecord::Migration
  def change
    add_column :fabrics, :material, :string
    add_column :fabrics, :density, :string
    add_column :fabrics, :collection, :text
  end
end
