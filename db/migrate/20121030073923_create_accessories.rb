class CreateAccessories < ActiveRecord::Migration
  def change
    create_table :accessories do |t|
      t.string :title, :null => false
      t.text :description
      t.string :category
      t.boolean :publish, :null => false, :default => 1
      t.string :material
      t.integer :price
      t.string :photo

      t.timestamps
    end
  end
end
