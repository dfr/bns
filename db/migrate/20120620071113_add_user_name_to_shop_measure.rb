class AddUserNameToShopMeasure < ActiveRecord::Migration
  def change
    add_column :shop_measures, :user_name, :string
  end
end
