class AddDateIndexToShopMeasure < ActiveRecord::Migration
  def change
    add_index :shop_measures, [:shop_id,:at ], :unique => true
  end
end
